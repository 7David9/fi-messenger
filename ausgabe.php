<?php
// $pdo = new PDO('mysql:host=localhost; dbname=fi-messenger', 'root', '');
$pdo = new PDO('mysql:host=localhost; dbname=fi-messenger', 'David', 'aA1234Aa.');
$pdo->query("SET NAMES 'utf8mb4'");

$sql = $pdo->prepare("SELECT message, username, DATE_FORMAT(date, '%d.%m.%Y') as datum, DATE_FORMAT(date, '%H:%i:%s') as uhr FROM messages ORDER BY id ASC");
$sql->execute();
$messages = [];

while($row = $sql->fetch()) {
	array_push($messages, array(
		"name" => htmlentities($row['username']),
		"msg" => htmlentities($row['message']),
		"datum" => htmlentities($row['datum']),
		"uhr" => htmlentities($row['uhr'])
	));
}
echo json_encode($messages);
?>