<?php
// $pdo = new PDO('mysql:host=localhost; dbname=fi-messenger', 'root', '');
$pdo = new PDO('mysql:host=localhost; dbname=fi-messenger', 'David', 'aA1234Aa.');
$pdo->query("SET NAMES 'utf8mb4'");

if (trim($_POST["text"]) !== "") {
	$daten = array($_POST["text"], $_POST["name"]);
	$sql = $pdo->prepare("SELECT count(Timestamp) FROM spammer WHERE Username = ? AND Timestamp < curtime() - INTERVAL 5 SECOND");
	$sql->execute([$daten[1]]);
	$spam = $sql->fetchColumn();
	
	if ($spam != 0) {
		$sql = $pdo->prepare("DELETE FROM spammer WHERE Username = ?");
		$sql->execute([$daten[1]]);
		
		// Bereinige Tabelle, wenn leer (Setze Index zurück) UND automatisch am nächsten Tag.
		$sql = $pdo->prepare("SELECT count(0) FROM spammer WHERE curdate() = date(Timestamp)");
		$sql->execute();
		$spam = $sql->fetchColumn();
		
		if($spam == 0) {
			$sql = $pdo->prepare("TRUNCATE `fi-messenger`.spammer");
			$sql->execute();
		}
		echo "Gelöscht / ";
	}
	$sql = $pdo->prepare("SELECT count(Timestamp) FROM spammer WHERE Username = ?");
	$sql->execute([$daten[1]]);
	$spam = $sql->fetchColumn();
	if ($spam == 0) {
		// Schreibe Nachricht
		$sql = $pdo->prepare("INSERT INTO messages (message, username) VALUES(?, ?)");
		$sql->execute($daten);
		// Warte 5 Sekunden
		$sql = $pdo->prepare("INSERT INTO spammer (Username) VALUES (?)");
		$sql->execute([$daten[1]]);
	}
	echo "Warte";
	// echo " / Eintrag geschrieben";
}
?>