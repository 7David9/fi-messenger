function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function setUsername() {
	// var a = localStorage.username_msgFI;
	var a = "";
	do {
		a = "";
		a = a.trim() ? a : prompt('Bitte gib deinen Namen ein: ', localStorage.username_msgFI);
		if (a == null) a = "";
	} while (!a.trim().length || a.trim().length > 64);
	localStorage.username_msgFI = a;
	document.querySelector('input[name=text]').focus();
	document.querySelector('input[name=name]').value = localStorage.username_msgFI;
}

function update_name() {
	var a = document.querySelector('input[name=name]').value;
	if (a.trim() !== "") {
		localStorage.username_msgFI = a;
	}
}

function update() {
	
	// ausgabe.contentWindow.location.reload(true);
	fetch("ausgabe.php").then(data=>data.json()).then(messages=>{
		var err = messages[messages.length-1];
		if (err && err.error) {
			console.log(err);
			messages.pop();
		}
		// console.log("M:", messages); // .message);
		var eintraege = document.createElement('div');
		eintraege.className = "eintraege";
		var i = 0;
		for (message of messages) {
			i++;
			var eintrag = document.createElement('div');
			eintrag.className = "eintrag";
			eintrag.innerHTML = "| Eintrag-Nr.: <b>" + i + "</b> |<br /> Benutzer <a href='#' name='username'><b>" + message.name + "</b></a> schrieb am<br />" +
								message.datum + " um " + message.uhr + " Uhr <a href='#' name='usermsg'><p>" + message.msg + "</p></a>";
			eintraege.appendChild(eintrag);
		}
		ausgabe.innerHTML = '';
		ausgabe.appendChild(eintraege);
		if (box.checked) try {
			document.querySelector('.eintrag:last-of-type').scrollIntoView();
			// info.innerText = "";
		} catch(e) {
			if (document.querySelectorAll('.eintrag').length === 0) {
				info.innerText = "Schreibe die erste Nachricht.";
			} else {
				info.innerText = "";
			}
		}
	});
}

document.querySelector('input[name=name]').onchange = update_name;

var itv; // Intervall
addEventListener('load', setUsername);
addEventListener('load', e=>{ update(); itv = setInterval(update, 1000) });

onsubmit = function(e) {
	e.preventDefault();
	if (document.querySelector('input[name=name]').value.trim() === "") {
		info.innerText = "Der Name darf nicht leer sein.";
		return;
	}
	var formElement = document.querySelector('#eingabe');
	fetch("eingabe.php", {
		method: 'POST',
		body: new URLSearchParams(new FormData(formElement))
	})
	.then(data=>data.text())
	.then(text=>{
		// console.log(text);
		if (!text) {
			info.innerText = "Bitte etwas schreiben!";
			return;
		} else if (text === "Warte") {
			info.innerText = "Etwas Geduld bitte!";
			document.querySelector('input[name=text]').disabled = true;
			setTimeout(function() {
				info.innerText = "";
				document.querySelector('input[name=text]').disabled = false;
				document.querySelector('input[name=text]').focus();
			}, 1500);
			// await sleep(1500);
			return 1;
		} else info.innerText = "";
		document.querySelector('input[name=text]').value='';
		update();
	});
}
